package main

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type MessageBody struct {
	BotID   string `json:"bot_id"`
	Message string `json:"text"`
}

type GroupmeBot struct {
	BotID string `json:"BotID"`
}

func (G *GroupmeBot) sendMessage(text string) error {
	URL := "https://api.groupme.com/v3/bots/post"
	body := MessageBody{
		BotID:   G.BotID,
		Message: text,
	}
	jBody, err := json.Marshal(body)
	if err != nil {
		return err
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(jBody))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	_, err = client.Do(req)
	if err != nil {
		return err
	}
	return nil
}
