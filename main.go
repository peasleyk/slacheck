package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type Status struct {
	Message string
	Down    bool
}

type Service struct {
	Description string `json:"Description"`
	Host        string `json:"Host"`
	Port        int    `json:"Port"`
	Machine     string `json:"Machine"`
	Sent        bool
}

type Configuration struct {
	Groupme struct {
		Token   string `json:"Code"`
		GroupID string `json:"Group"`
		BotID   string `json:"BotID"`
	}
	Services map[string]Service `json:"Services"`
}

func loadConfig(location string) (Configuration, error) {
	configuration := Configuration{}
	file, err := os.Open(location)
	if err != nil {
		return configuration, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration.Services = make(map[string]Service)
	err = decoder.Decode(&configuration)
	if err != nil {
		return configuration, err
	}
	return configuration, nil
}

// Works by executing nmap on each service and checking the output lazily
// for a few key phrases
// If this is too slow, we can run the script on the VMs and use
// something like "service <x> status"
func check(name string, service Service) Status {
	var status Status
	out, _ := exec.Command("nmap", service.Host, "-p", strconv.Itoa(service.Port)).Output()
	if strings.Contains(string(out), "closed") {
		status.Message = fmt.Sprintf("Service %s down on %s:%d, %s!", name, service.Host, service.Port, service.Machine)
		status.Down = true
	} else if strings.Contains(string(out), "Host seems down") {
		status.Message = fmt.Sprintf("Host %s down!", service.Host)
		status.Down = true
	} else {
		status.Message = fmt.Sprintf("Service %s up on %s:%d, %s", name, service.Host, service.Port, service.Machine)
		status.Down = false
		service.Sent = false
	}
	return status
}

func main() {
	mode := flag.String("mode", "debug", "Print messages to groupme (live) or console (debug)")
	flag.Parse()

	location := "config.json"
	configuration, err := loadConfig(location)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	bot := GroupmeBot{
		BotID: configuration.Groupme.BotID,
	}
	serviceMessageStatus := make(map[string]bool)
	for {
		for name, service := range configuration.Services {
			status := check(name, service)
			if status.Down == true {
				if serviceMessageStatus[name] == false {
					serviceMessageStatus[name] = true
					if *mode == "debug" {
						fmt.Println(status.Message)
					} else {
						bot.sendMessage(status.Message)
					}
				}
			} else if serviceMessageStatus[name] == true {
				if *mode == "debug" {
					fmt.Println(status.Message)
				} else {
					bot.sendMessage(status.Message)
				}
				serviceMessageStatus[name] = false
			} else {
				serviceMessageStatus[name] = false

			}
		}
		time.Sleep(5 * time.Second)

	}
}
