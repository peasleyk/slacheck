# Service checker

Simple service checker that notifies if a certain service (say apache) is not responding, or if a whole host is down.

This was made for my final in Cyber Defence, for a Red vs. Blue exercise.

### Setting up group me
1. Go here and make a bot to get a token
- https://dev.groupme.com/bots/new
2. Get your rooms groupId
4. Send a request to join the room
```shell
curl -X POST -d '{"bot": { "name": "Bot name", "group_id": "123456"}}' -H 'Content-Type: application/json' https://api.groupme.com/v3/bots?token=<token from step 1>
```
This returns a bot token for that group. Use that in config.json
```json
{
  meta: {
    "code": 201
  },
  response: {
    bot: {
      name: "Bot name",
      bot_id: "<bot Id to put in config.json",
      //...
    }
  }
}
```

### Setting up services

```config.json``` holds all the services to check, they will be checked sequentially.

Here's an entry example:
```json
"FTP": {
  "Port": 9090,
  "Host": "192.234.234.221",
  "Description": "FTP service"
}
```
Description isn't used for now.

For every entry the script will call ```nmap <host>:<port>``` and based on the return, either say that
  - Service is down
  - Host is down
  - Everything is OK

### Building
##### Go (native)
To build and run:
```shell
go build
./sla-checker -help
Usage of ./sla-checker:
  -mode string
      Print messages to groupme (live) or console (debug) (default "debug")
./sla-checker -mode live
```
live will send to group me, debug will print out message

##### Docker
If you don't have go installed but have docker build and run it:
```shell
docker-compose up
```
This will start it in live mode, with config volumed it. This way you can just delete the container then re-run ```docker-compose up``` if a service is added