FROM golang:latest AS builder

COPY bot.go /app/
COPY main.go /app/
#COPY config.json /app/

WORKDIR /app
RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o sla

FROM alpine
COPY --from=builder /app/ /app/
WORKDIR /app/
CMD ["./sla", "-mode", "live"]
